//
//  Extension+ViewController.swift
//  teste-mobile
//
//  Created by cedro_nds on 26/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit

extension UIViewController {

    func hideKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
