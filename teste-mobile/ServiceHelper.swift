//
//  ServiceHelper.swift
//  teste-mobile
//
//  Created by Thiago Henrique Pereira Freitas on 23/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit
import Alamofire
<<<<<<< HEAD
import GoogleAPIClientForREST
import GoogleSignIn


class ServiceHelper: NSObject {

    var videoTitle = ""
    
    
    func getVideos(videoTitle : String, handler: @escaping (_ msg: DataResponse<Video>) -> ()){
        
        self.videoTitle = videoTitle
        let baseUrl = "https://www.googleapis.com/youtube/v3/search?part=id,snippet&q=\(videoTitle)&key=AIzaSyD745vMAYPp6aDyRMBpECm_ZzFErDQ_6yM"
        
        //Alamofire.request(baseUrl, method: .get, parameters: <#T##Parameters?#>, encoding: <#T##ParameterEncoding#>, headers: <#T##HTTPHeaders?#>)
        Alamofire.request(baseUrl).responseJSON { response in
            if let JSON = response.result.value as? Video{
                print(JSON)
            }
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
            }
        }
        /*Alamofire.request(baseUrl).responseObject { (response: DataResponse<Video>) in
            
            switch response.result {
                
            case .success:
                
                handler(response)
                
            case .failure(let error):
                print(error)
            }
        }*/
=======
import AlamofireObjectMapper

class ServiceHelper: NSObject {

    var videoTitle = " "
    let key = "&key=AIzaSyCU5Z8xta1eG3nirRoTCIqEJ2-oQ2yJ4-A"
    let baseUrl = "https://www.googleapis.com/youtube/v3/"

    func getVideos(videoTitle : String, nextPageToken: String, handler: @escaping (_ msg: DataResponse<Video>) -> ()){
        
        self.videoTitle = videoTitle
        var getVideosUrl = "search?part=id,snippet&q="
        
        if nextPageToken != "" {
            getVideosUrl = "search?pageToken=\(nextPageToken)&part=id,snippet&q="
        }
        let url = self.baseUrl+getVideosUrl+videoTitle+key
        
        Alamofire.request(url).responseObject { (response: DataResponse<Video>) in
            switch response.result {
                case .success:
                    handler(response)
                case .failure(let error):
                    print(error)
            }
        }
    }
    
    func getVideo(videoId : String, handler: @escaping (_ msg: DataResponse<Video>) -> ()){
        let urlPart = "&part=snippet,statistics"
        let url = self.baseUrl+"videos?id="+videoId+urlPart+key
        
        Alamofire.request(url).responseObject { (response: DataResponse<Video>) in
            switch response.result {
                case .success:
                    handler(response)
                case .failure(let error):
                    print(error)
            }
        }
>>>>>>> 123748ac2b3e041173445be6582616904717b472
    }
}
