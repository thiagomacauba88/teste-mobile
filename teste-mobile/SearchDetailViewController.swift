//
//  SearchDetailViewController.swift
//  teste-mobile
//
//  Created by Thiago Henrique Pereira Freitas on 25/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit
import AlamofireImage
import YouTubeFloatingPlayer
import KRProgressHUD

class SearchDetailViewController: UIViewController{
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var descriptionTextView: UITextView!
    var videoId :String?
    @IBOutlet var viewCountsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.webView.delegate = self
        loadYoutube(videoID: self.videoId!)
        self.setupPageLayout()
        self.getVideo(videoId: self.videoId!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func loadYoutube(videoID:String) {
        guard
            let youtubeURL = URL(string: "https://www.youtube.com/embed/\(videoID)")
            else { return }
        webView.loadRequest( URLRequest(url: youtubeURL) )
        
    }
    func getVideo(videoId : String) {
        KRProgressHUD.show()
        ServiceHelper().getVideo(videoId: videoId, handler: {
            (videoList) in
            
            self.setupComponents(item: (videoList.value?.items?.first)!)
        })
    }

    func setupPageLayout() {
        
        let logo = UIImage(named: "logoImage")
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit // set imageview's content mode
        self.navigationItem.titleView = imageView
    }
    
    func setupComponents(item: Items) {
        self.titleLabel.text = item.snippet?.title
        self.descriptionTextView.text = item.snippet?.descriptionName
        self.viewCountsLabel.text = (item.statistics?.viewCount)!+" visualizações"
    }
}

extension SearchDetailViewController : UIWebViewDelegate {
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.imageView.isHidden = true
            KRProgressHUD.dismiss()
        }
        return true
    }
}

