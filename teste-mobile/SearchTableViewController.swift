//
//  SearchTableViewController.swift
//  teste-mobile
//
//  Created by Thiago Henrique Pereira Freitas on 23/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit
<<<<<<< HEAD

class SearchTableViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageLayout()
        // Do any additional setup after loading the view.
    }

    func setupPageLayout() {

=======
import AlamofireImage
import KRProgressHUD

class SearchTableViewController: UIViewController {

    var videoList:Video?
    var videoId:String?
    var videoSelected: String = ""
    var nextPageToken:String = ""
    
    @IBOutlet var textField: UITextField!
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.layoutIfNeeded()
        self.hideKeyboard()
        self.setupPageLayout()
        self.textField.setBottomBorder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func setupPageLayout() {
>>>>>>> 123748ac2b3e041173445be6582616904717b472
        let logo = UIImage(named: "logoImage")
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit // set imageview's content mode
        self.navigationItem.titleView = imageView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
<<<<<<< HEAD
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
=======
    }
    
    @IBAction func findVideos(_ sender: Any) {
        if self.textField.text == "" {
            self.view.makeToast("Digite o nome de algum vídeo")
        } else {
            let videoName = self.textField.text?.replacingOccurrences(of: " ", with: "")
            self.nextPageToken = ""
            KRProgressHUD.show()
            self.getVideosList(videoName: videoName!, nextPageToken: self.nextPageToken)
        }
    }
    
    func getVideosList(videoName : String, nextPageToken: String) {
        ServiceHelper().getVideos(videoTitle: videoName, nextPageToken: nextPageToken, handler: {
            (videoList) in
            if nextPageToken != "" {
                for item in (videoList.value?.items)! {
                    self.videoList?.items?.append(item)
                }
                self.nextPageToken = (videoList.value?.nextPageToken)!
            } else {
                self.videoList = videoList.value
                self.nextPageToken = (self.videoList?.nextPageToken)!
            }
            self.tableView.reloadData()
            KRProgressHUD.dismiss()
        })
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)) {
            KRProgressHUD.show()
            self.getVideosList(videoName: self.videoSelected, nextPageToken: self.nextPageToken)
        }
    }
}

extension SearchTableViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.videoList?.items?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let item = self.videoList?.items?[indexPath.row]
        let imageView = cell.contentView.viewWithTag(1) as! UIImageView
        let titleLabel = cell.contentView.viewWithTag(2) as! UILabel
        let descriptionTextView = cell.contentView.viewWithTag(3) as! UITextView
        titleLabel.text = item?.snippet?.title
        descriptionTextView.text = item?.snippet?.descriptionName
        let downloadURL = NSURL(string: (item?.snippet?.thumbnails?.high?.url)!)
        imageView.af_setImage(withURL: downloadURL! as URL)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.videoId = self.videoList?.items?[indexPath.row].id?.videoId
        self.performSegue(withIdentifier: "segueSearchTableViewDetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueSearchTableViewDetail" {
            let searchDetailViewController = segue.destination as! SearchDetailViewController
            searchDetailViewController.videoId = self.videoId
        }
    }
 }
>>>>>>> 123748ac2b3e041173445be6582616904717b472
