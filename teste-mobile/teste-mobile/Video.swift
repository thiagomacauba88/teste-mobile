//
//  Video.swift
//  teste-mobile
//
//  Created by Thiago Henrique Pereira Freitas on 23/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit
import ObjectMapper

class Video: Mappable {
    var pageInfo : PageInfo?
    var items: [Items]?
    var nextPageToken: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        pageInfo <- map["pageInfo"]
        items <- map["items"]
        nextPageToken <- map["nextPageToken"]
    }
}

class PageInfo: Mappable {
    var totalResults : Int?
    var resultsPerPage : Int?
    var description: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        totalResults <- map["totalResults"]
        resultsPerPage <- map["resultsPerPage"]
    }
}

class Items: Mappable {
    var id : IdVideo?
    var snippet : Snippet?
    var statistics : Statistics?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        snippet <- map["snippet"]
        id <- map["id"]
        statistics <- map["statistics"]
    }
}

class IdVideo: Mappable {
    var videoId : String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        videoId <- map["videoId"]
    }
}

class Statistics: Mappable {
    var viewCount : String?
    
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        viewCount <- map["viewCount"]
    }
}


class Snippet: Mappable {
    var descriptionName : String?
    var title : String?
    var thumbnails : ThumbNails?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        descriptionName <- map["description"]
        title <- map["title"]
        thumbnails <- map["thumbnails"]
    }
}
class ThumbNails: Mappable {
    var high : High?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        high <- map["high"]
    }
}

class High: Mappable {
    var url : String?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        url <- map["url"]
    }
}
