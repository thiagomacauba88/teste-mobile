//
//  ServiceHelper.swift
//  teste-mobile
//
//  Created by Thiago Henrique Pereira Freitas on 23/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ServiceHelper: NSObject {

    var videoTitle = " "
    let key = "&key=AIzaSyCU5Z8xta1eG3nirRoTCIqEJ2-oQ2yJ4-A"
    let baseUrl = "https://www.googleapis.com/youtube/v3/"

    func getVideos(videoTitle : String, nextPageToken: String, handler: @escaping (_ msg: DataResponse<Video>) -> ()){
        
        self.videoTitle = videoTitle
        var getVideosUrl = "search?part=id,snippet&q="
        
        if nextPageToken != "" {
            getVideosUrl = "search?pageToken=\(nextPageToken)&part=id,snippet&q="
        }
        let url = self.baseUrl+getVideosUrl+videoTitle+key
        
        Alamofire.request(url).responseObject { (response: DataResponse<Video>) in
            switch response.result {
                case .success:
                    handler(response)
                case .failure(let error):
                    print(error)
            }
        }
    }
    
    func getVideo(videoId : String, handler: @escaping (_ msg: DataResponse<Video>) -> ()){
        let urlPart = "&part=snippet,statistics"
        let url = self.baseUrl+"videos?id="+videoId+urlPart+key
        
        Alamofire.request(url).responseObject { (response: DataResponse<Video>) in
            switch response.result {
                case .success:
                    handler(response)
                case .failure(let error):
                    print(error)
            }
        }
    }
}
