//
//  Extension+UITextField.swift
//  teste-mobile
//
//  Created by cedro_nds on 26/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit

extension UITextField {

    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 33/255, green: 123/255, blue: 224/255, alpha: 1).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
