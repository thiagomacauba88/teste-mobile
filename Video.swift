//
//  Video.swift
//  teste-mobile
//
//  Created by Thiago Henrique Pereira Freitas on 23/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit
import ObjectMapper

class Video: Mappable {
    var pageInfo : PageInfo?
    //var items: [Items]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        pageInfo <- map["pageInfo"]
        //items <- map["items"]
    }
}

class PageInfo: Mappable {
    var totalResults : Int?
    var resultsPerPage : Int?
    //var description: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        totalResults <- map["totalResults"]
        resultsPerPage <- map["resultsPerPage"]
    }
}
/*
class Items: Mappable {
    var snippet : Snippet?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        snippet <- map["snippet"]
    }
}

class Snippet: Mappable {
    var descriptionName : String?
    var title : String?
    var thumbnails : ThumbNails?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        descriptionName <- map["descriptionName"]
        title <- map["title"]
        thumbnails <- map["thumbNails"]
    }
}
class ThumbNails: Mappable {
    var defaultValue : Default?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        defaultValue <- map["default"]
    }
}

class Default: Mappable {
    var url : String?
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        url <- map["url"]
    }
}*/
