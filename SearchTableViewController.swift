//
//  SearchTableViewController.swift
//  teste-mobile
//
//  Created by Thiago Henrique Pereira Freitas on 23/10/17.
//  Copyright © 2017 MobiMais. All rights reserved.
//

import UIKit

class SearchTableViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageLayout()
        // Do any additional setup after loading the view.
    }

    func setupPageLayout() {

        let logo = UIImage(named: "logoImage")
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit // set imageview's content mode
        self.navigationItem.titleView = imageView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
